using AutoFixture;
using FullStack.API.Controllers;
using FullStack.API.Data;
using FullStack.API.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace FullStack.Tests
{
    [TestClass]
    public class EmployeeControllerTests
    {
        private Mock<IFullStackRepository> _repository;
        private Fixture _fixture;
        private EmployeesController _employeesController;

        public EmployeeControllerTests()
        {
            _fixture = new Fixture();
            _repository = new Mock<IFullStackRepository>();
        }


        [TestMethod]
        public async Task Get_Employees_ReturnOk()
        {
            //Arange 
            var employees = _fixture.CreateMany<Employee>(4).ToList();
            _repository.Setup(repo => repo.GetAllEmployees()).Returns(employees);
            _employeesController = new EmployeesController(_repository.Object);

            //Act
            var result = await _employeesController.GetAllEmployees();
            var obj = result as ObjectResult;

            //Asert
            Assert.AreEqual(200, obj.StatusCode);
        }
    }
}