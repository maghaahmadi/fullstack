﻿using FullStack.API.Models;

namespace FullStack.API.Data
{
    public interface IFullStackRepository
    {
        IList<Employee> GetAllEmployees();
        Employee GetEmployee(int id);
        Employee AddEmployee(Employee employee);
        Employee UpdateEmployee(Employee employee);
        Employee DeleteEmployee(int id);

    }
    public class FullStackRepository : IFullStackRepository
    {
        private readonly FullStackDBContext _dbContext;

        public FullStackRepository(FullStackDBContext dbContext)
        {

            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));

        }
        public Employee AddEmployee(Employee employee)
        {
            _dbContext.Employees.Add(employee);
            return employee;
        }

        public Employee? DeleteEmployee(int id)
        {
            var currentEmployee = _dbContext.Employees.Find(id);

            if (currentEmployee == null) return null;

            _dbContext.Employees.Remove(currentEmployee);
            _dbContext.SaveChanges();

            return currentEmployee;
        }

        public IList<Employee> GetAllEmployees()
        {
            return _dbContext.Employees.ToList();
        }

        public Employee GetEmployee(int id)
        {
            return _dbContext.Employees.Find(id);
        }

        public Employee? UpdateEmployee(Employee employee)
        {
            var employeeId = employee.Id;
            var currentEmployee = _dbContext.Employees.Find(employeeId);

            if (currentEmployee == null) return null;

            currentEmployee.Name = employee.Name;
            currentEmployee.Phone = employee.Phone;
            currentEmployee.Email = employee.Email;
            currentEmployee.Salary = employee.Salary;
            currentEmployee.Department = employee.Department;

            _dbContext.SaveChanges();

            return currentEmployee;
        }
    }
}
