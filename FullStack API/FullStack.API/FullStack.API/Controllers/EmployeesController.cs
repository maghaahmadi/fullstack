﻿using FullStack.API.Data;
using FullStack.API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;

namespace FullStack.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IFullStackRepository _fullStackRepository;
        public EmployeesController(IFullStackRepository fullStackRepository)
        {

            _fullStackRepository = fullStackRepository ?? throw new ArgumentNullException(nameof(fullStackRepository));

        }
        [HttpGet]
        public async Task<IActionResult> GetAllEmployees()
        {
            var result = _fullStackRepository.GetAllEmployees();
            return Ok(result);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Employee>> GetEmployee(int id)
        {
            var result = _fullStackRepository.GetEmployee(id);
            if (result == null) return NotFound();
            return result;
        }

        [HttpPost]
        public async Task<IActionResult> AddEmployee([FromBody] Employee employee)
        {
            _fullStackRepository.AddEmployee(employee);
            return Ok(employee);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateEmployee([FromBody] Employee employee)
        {
            var currentEmployee = _fullStackRepository.UpdateEmployee(employee);
            if (currentEmployee == null) return NotFound();
            return Ok(employee);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteEmployee(int id)
        {
            var currentEmployee = _fullStackRepository.DeleteEmployee(id);
            if (currentEmployee == null) return NotFound();
            return Ok(currentEmployee);
        }
    }
}
